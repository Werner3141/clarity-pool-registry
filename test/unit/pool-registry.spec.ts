import { Client, Provider, ProviderRegistry } from "@blockstack/clarity";
import chai, { expect } from "chai";
import chaiString from "chai-string";
chai.use(chaiString);

describe("pool registry contract test suite", () => {
  let client: Client;
  let mockPoxClient: Client;
  let mockPoxExtClient: Client;
  let mockBnsClient: Client;
  let provider: Provider;

  before(async () => {
    provider = await ProviderRegistry.createProvider();
    client = new Client(
      "SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB.pool-registry",
      "pool-registry",
      provider
    );
    mockPoxClient = new Client(
      "SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB.mock-pox",
      "mock-pox",
      provider
    );
    mockPoxExtClient = new Client(
      "SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB.mock-pox-ext",
      "mock-pox-ext",
      provider
    );
    mockBnsClient = new Client(
      "ST000000000000000000002AMW42H.bns",
      "mock-bns",
      provider
    );
  });

  it("should have a valid syntax", async () => {
    await mockBnsClient.deployContract();
    await client.checkContract();
  });

  describe("deploying an instance of the contract", () => {
    before(async () => {
      await mockPoxClient.deployContract();
      await mockPoxExtClient.deployContract();
      await client.deployContract();
    });

    it("should add BTC", async () => {
      const tx = client.createTransaction({
        method: {
          name: "add-payout",
          args: ['"BTC"', '"Bitcoin"'],
        },
      });
      tx.sign("SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB");
      const receipt = await client.submitTransaction(tx);
      expect(receipt.success, JSON.stringify(receipt)).to.be.true;
    });

    it("should register", async () => {
      const tx = client.createTransaction({
        method: {
          name: "register",
          args: [
            "{namespace: 0x1234, name: 0x1234}",
            "'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB",
            "(list {hashbytes: 0x1234, version: 0x00})",
            '"https://example.com"',
            "'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB.mock-pox",
            "none",
            "(list u1)",
            '"BTC"',
            '""',
            '""',
            "u1",
          ],
        },
      });
      tx.sign("SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB");
      const receipt = await client.submitTransaction(tx);
      expect(receipt.success, JSON.stringify(receipt)).to.be.true;
      expect(receipt.result).to.startWith(
        "Transaction executed and committed. Returned: u1"
      );
    });

    it("should register extended", async () => {
      const tx = client.createTransaction({
        method: {
          name: "register",
          args: [
            "{namespace: 0x0007, name: 0x1234}",
            "'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB.ext",
            "(list {hashbytes: 0x1234, version: 0x00})",
            '"https://example.com"',
            "'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB.mock-pox-ext",
            "none",
            "(list)",
            '"BTC"',
            '""',
            '""',
            "u1",
          ],
        },
      });
      tx.sign("SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB");
      const receipt = await client.submitTransaction(tx);
      expect(receipt.success, JSON.stringify(receipt)).to.be.true;
      expect(receipt.result).to.startWith(
        "Transaction executed and committed. Returned: u2"
      );
    });

    it("should update", async () => {
      const tx = client.createTransaction({
        method: {
          name: "update",
          args: [
            "{namespace: 0x1234, name: 0x1234}",
            "'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB",
            "(list {hashbytes: 0x9876, version: 0x00})",
            '"https://example.com"',
            "'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB.mock-pox",
            "none",
            "(list u2)",
            '"BTC"',
            '""',
            '""',
            "u1",
          ],
        },
      });
      tx.sign("SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB");
      const receipt = await client.submitTransaction(tx);
      expect(receipt.success, JSON.stringify(receipt)).to.be.true;
      expect(receipt.result).to.startWith(
        "Transaction executed and committed. Returned: true"
      );
    });

    it("should not update on other's name", async () => {
      const tx = client.createTransaction({
        method: {
          name: "update",
          args: [
            "{namespace: 0x1234, name: 0x1234}",
            "'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB",
            "(list {hashbytes: 0x9876, version: 0x00})",
            '"https://example.com"',
            "'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB.mock-pox",
            "none",
            "(list u2)",
            '"BTC"',
            '""',
            '""',
            "u1",
          ],
        },
      });
      tx.sign("SP3W7P3S6HEHR9ZJ91DZEC683WFYCDACNHVWSAN5K");
      const receipt = await client.submitTransaction(tx);
      expect(receipt.success, JSON.stringify(receipt)).to.be.false;
      expect(receipt.error.commandOutput).to.startWith("Aborted: u5");
    });

    it("should update extended", async () => {
      const tx = client.createTransaction({
        method: {
          name: "update-ext",
          args: [
            "{namespace: 0x1234, name: 0x1234}",
            "'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB.ext",
            "(list {hashbytes: 0x9876, version: 0x00})",
            '"https://example.com"',
            "'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB.mock-pox-ext",
            "none",
            "(list)",
            '"BTC"',
            '""',
            '""',
            "u1",
          ],
        },
      });
      tx.sign("SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB");
      const receipt = await client.submitTransaction(tx);
      expect(receipt.success, JSON.stringify(receipt)).to.be.true;
      expect(receipt.result).to.startWith(
        "Transaction executed and committed. Returned: true"
      );
    });

    it("should get a list of pools", async () => {
      const q = await client.createQuery({
        method: { name: "get-pools", args: ["(list u1 u2 u3)"] },
      });
      const receipt = await client.submitQuery(q);
      expect(receipt.success).to.be.true;
    });
  });

  after(async () => {
    await provider.close();
  });
});
