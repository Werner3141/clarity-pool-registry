(define-read-only (name-resolve (namespace (buff 20)) (name (buff 48)))
  (if (is-eq tx-sender 'SP3GWX3NE58KXHESRYE4DYQ1S31PQJTCRXB3PE9SB)
    (ok {lease-ending-at: (+ u10000 block-height), lease-started-at: (- u10000 block-height),
     owner: tx-sender, zonefile-hash: 0x1234})
    (err 1)))

(define-public (name-preorder (hashed-salted-fqn (buff 20))
                              (stx-to-burn uint))
  (if true
    (ok (+ block-height u100))
    (err 1)))


(define-public (name-register (namespace (buff 20))
                              (name (buff 48))
                              (salt (buff 20))
                              (zonefile-hash (buff 20)))
  (if true
    (ok true)
    (err 1)))

(define-read-only (can-receive-name (owner principal))
  (if true
    (ok true)
    (err 1)))

(define-read-only (can-name-be-registered (namespace (buff 20)) (name (buff 48)))
  (if true
    (ok true)
    (err 1)))

(define-read-only (get-name-price (namespace (buff 20)) (name (buff 48)))
  (if true
    (ok u1000)
    (err 1)))