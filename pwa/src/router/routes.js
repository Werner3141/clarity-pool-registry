const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      { path: "registry", component: () => import("pages/Registry.vue") },
      { path: "delegating", component: () => import("pages/Delegating.vue") },
      { path: "add", component: () => import("pages/AddPool.vue") }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
