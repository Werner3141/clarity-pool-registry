import { network } from "../src/deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
  downloadBoomBoxesTxs,
  logBoomBoxesContractCSV,
} from "../src/pool-tool-utils";

(async () => {
  await downloadBoomBoxesTxs(network);
  await logBoomBoxesContractCSV(network);
})();
