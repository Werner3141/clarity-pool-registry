import { network } from "../src/deploy";
import { logPoxContractCSV, downloadPoxTxs } from "../src/pool-tool-utils";

(async () => {
  await downloadPoxTxs(network);
  logPoxContractCSV(network);
})();
