import { network } from "../src/deploy";
import {
  downloadPoxTxs,
  logDelegationStatesCSV,
  writeDelegationStates,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;

(async () => {
  //const txs = await downloadPoxTxs(network);
  //await writeDelegationStates(txs, pool700.stacks, 8715);
  console.log("stacker, stacked, amount, admin, untilBurnHt");
  await logDelegationStatesCSV(poolAdmin);
  await logDelegationStatesCSV(pool3cycles);
  await logDelegationStatesCSV(pool6cycles);
  await logDelegationStatesCSV(poolCcycles);
  await logDelegationStatesCSV(pool700);
})();
