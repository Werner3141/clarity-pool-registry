import { cvToString } from "@stacks/transactions";
import { readFileSync } from "fs";
import { mainnet, network } from "../src/deploy";
import {
  downloadPoxTxs,
  infoApi,
  logDelegationStatesCSV,
  stackDelegatedStxsInBatches,
  writeDelegationStates,
} from "../src/pool-tool-utils";
import { poxAddrCV, poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;

const rewardPoxAddrCV = poxAddrCVFromBitcoin(
  "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq"
  //"1AJm7XWhbPTFqgtJT6oS9wQ3XThk7rA2yF"
);

(async () => {
  const length = 15;
  // Change here start
  const indices = undefined;
  const lockingPeriod = 6;
  const admin = pool6cycles;
  const minUntilBurnHt = 689050; //682750 //678550;
  // change here end
  const info = await infoApi.getCoreApiInfo();
  console.log(info.burn_block_height);
  const startBurnHeight = 676300; //info.burn_block_height + 3; // + 1 or more, just in case the node is behind.
  const cycleId = mainnet ? 5 : 124;

  console.log(cvToString(rewardPoxAddrCV));

  await stackDelegatedStxsInBatches(
    indices,
    length,
    rewardPoxAddrCV,
    startBurnHeight,
    lockingPeriod,
    minUntilBurnHt,
    cycleId,
    admin
  );
})();
