import { AccountsApi } from "@stacks/blockchain-api-client";
import {
  bufferCV,
  callReadOnlyFunction,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  makeSTXTokenTransfer,
  standardPrincipalCV,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { network, handleTransaction } from "../src/deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
  accountsApi,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const BN = require("bn.js");

const poolAdmin = keys.poolAdmin;

async function payoutSet(
  stackersSet: { stacker: string; reward: number }[],
  totalPayoutOfSet: number,
  nonce?: number
) {
  const tx = await makeContractCall({
    contractAddress: "SP3FBR2AGK5H9QBDH3EEN6DF8EK8JY7RX8QJ5SVTE",
    contractName: "send-many-memo",
    functionName: "send-many",
    functionArgs: [
      listCV(
        stackersSet.map((s) =>
          tupleCV({
            to: standardPrincipalCV(s.stacker),
            ustx: uintCV(s.reward),
            memo: bufferCV(Buffer.from("reward cycle #5")),
          })
        )
      ),
    ],
    senderKey: poolAdmin.private,
    nonce: nonce ? new BN(nonce) : undefined,
    network,
    postConditions: [
      makeStandardSTXPostCondition(
        poolAdmin.stacks,
        FungibleConditionCode.Equal,
        new BN(totalPayoutOfSet)
      ),
    ],
  });
  const result = await handleTransaction(tx);
  console.log({ result });
}

async function verifyAndPayout(
  stackerSet: {
    rewardCycle: number;
    amount: number;
    stacker: string;
    reward: number;
  }[],
  nonce?: number
) {
  const totalPayoutSet = stackerSet.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  const totalStackedSet = stackerSet.reduce(
    (sum: number, s) => sum + s.amount,
    0
  );
  console.log(stackerSet.length, totalStackedSet, totalPayoutSet);
  await payoutSet(stackerSet, totalPayoutSet, nonce);
}

(async () => {
  const rewardCycle = 5;
  await downloadPoolToolV0Txs(network);
  await downloadPoolToolTxs(network);
  const v0Stackers = getStackersFromPoolTool(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
  console.log(v0Stackers.length);
  const v1Stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network
  );
  console.log(v1Stackers.length);
  const stackersInCycle = stackersToCycle(
    v0Stackers.concat(v1Stackers),
    rewardCycle
  );
  const stackers = stackersInCycle.members;
  const totalRewards = 19098085369;
  const totalStacked = stackers.reduce((sum: number, s) => sum + s.amount, 0);
  const extendedStackers = stackers.map((stacker) => {
    return {
      reward: Math.round((stacker.amount * totalRewards) / totalStacked),
      ...stacker,
    };
  });
  const totalPayout = extendedStackers.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  console.log(extendedStackers.length, totalStacked, totalPayout);
  let accountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });
  let nonce = accountInfo.nonce;
  const set1 = extendedStackers.slice(0, 200);
  await verifyAndPayout(set1);
  const set2 = extendedStackers.slice(200, 400);
  await verifyAndPayout(set2, nonce + 1);
  const set3 = extendedStackers.slice(400, 600);
  await verifyAndPayout(set3, nonce + 2);
  //const set4 = extendedStackers.slice(600);
  //await verifyAndPayout(set4, nonce + 3);
})();
