import { getPartialStacked } from "../src/pool-tool-partials";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;
const btcAddr1 = "1AJm7XWhbPTFqgtJT6oS9wQ3XThk7rA2yF";
const btcAddr2 = "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq";

(async () => {
  const cycleId = 6;
  console.log(await getPartialStacked(poolAdmin.stacks, cycleId, btcAddr1));
  console.log(await getPartialStacked(poolAdmin.stacks, cycleId, btcAddr2));
  console.log(await getPartialStacked(pool3cycles.stacks, cycleId, btcAddr1));
  console.log(await getPartialStacked(pool3cycles.stacks, cycleId, btcAddr2));
  console.log(await getPartialStacked(pool6cycles.stacks, cycleId, btcAddr1));
  console.log(await getPartialStacked(pool6cycles.stacks, cycleId, btcAddr2));
  console.log(await getPartialStacked(poolCcycles.stacks, cycleId, btcAddr1));
  console.log(await getPartialStacked(poolCcycles.stacks, cycleId, btcAddr2));
  console.log(await getPartialStacked(pool700.stacks, cycleId, btcAddr2));
})();
