import {
  contractPrincipalCV,
  makeContractCall,
  noneCV,
} from "@stacks/transactions";
import { handleTransaction, network } from "../src/deploy";
import { poolToolContract, poxContractAddress } from "../src/pool-tool-utils";
import { keys } from "./config";

const { pool700 } = keys;

(async () => {
  const tx = await makeContractCall({
    contractAddress: poxContractAddress,
    contractName: "pox",
    functionName: "allow-contract-caller",
    functionArgs: [
      contractPrincipalCV(poolToolContract.address, poolToolContract.name),
      noneCV(),
    ],
    senderKey: pool700.private,
    network: network,
  });
  await handleTransaction(tx);
})();
