import { readFileSync } from "fs";
import { mainnet } from "../src/deploy";

if (!mainnet) {
  throw new Error("Config only for mainnet");
}

const poolAdmin = JSON.parse(
  readFileSync(
    "../mainnet-keys.json"
  ).toString()
);
const pool3cycles = JSON.parse(
  readFileSync(
    "../mainnet-3-keys.json"
  ).toString()
);
const pool6cycles = JSON.parse(
  readFileSync(
    "../mainnet-6-keys.json"
  ).toString()
);
const poolCcycles = JSON.parse(
  readFileSync(
    "../mainnet-C-keys.json"
  ).toString()
);

const pool700 = JSON.parse(
  readFileSync(
    "../mainnet-keys-700.json"
  ).toString()
)
export const keys = {
  poolAdmin,
  pool3cycles,
  pool6cycles,
  poolCcycles,
  pool700,
};
